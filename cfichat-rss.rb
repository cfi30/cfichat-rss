#!/usr/bin/env ruby
require 'bundler/setup'
require 'cfichat/client'
require 'digest/sha1'
require 'fiber'
require 'htmlentities'
require 'rss'
require 'sqlite3'
require 'yaml'

Settings = YAML.load_file 'settings.yml'

class RSS_Bot < CfiChat::Client::Bot
  Feed = Struct.new :title, :items
  Item = Struct.new :title, :link

  def initialize(client)
    super

    @client = client
    @html = HTMLEntities.new
    @locked = false

    @db = SQLite3::Database.new 'subscriptions.db'
    @db.busy_timeout = 1000

    @db.execute <<-SQL
    CREATE TABLE IF NOT EXISTS feeds (
      id TEXT PRIMARY KEY COLLATE nocase,
      url TEXT,
      updated DATETIME DEFAULT CURRENT_TIMESTAMP,
      broadcast INTEGER DEFAULT 0
    )
    SQL

    @db.execute <<-SQL
    CREATE TABLE IF NOT EXISTS subs (
      id INTEGER PRIMARY KEY,
      feed TEXT COLLATE nocase,
      nick TEXT
    )
    SQL

    @db.execute <<-SQL
    CREATE TABLE IF NOT EXISTS cache (
      id INTEGER PRIMARY KEY,
      feed TEXT,
      guid TEXT
    )
    SQL

    @db.execute <<-SQL
    CREATE TABLE IF NOT EXISTS queue (
      id INTEGER PRIMARY KEY,
      nick TEXT,
      message TEXT
    )
    SQL

    update = proc do
      Fiber.new { fetch_updates }.resume
    end

    EM.add_periodic_timer Settings[:update_time] * 60, &update
    client.add_login_hook &update

    EM.add_periodic_timer Settings[:send_time] do
      wake
      read_queue
    end

    EM.add_shutdown_hook do
      next unless @locked

      debug 'eventmachine shutdown - update stopped'
      @db.execute "ROLLBACK"
    end

    @lang = {
      :bad_call         => 'oups: mauvais arguments',
      :big_notif        => '[quote=Nouvelles]',
      :broadcasted      => '[b]%s[/b]',
      :chotto_matte     => 'un instant...',
      :feed_added       => "j'ai ajouté le flux « %s » à ma liste :)",
      :feed_exists      => 'je connais déjà ce flux :P',
      :feed_invalid     => "je n'arrive pas à lire ce flux :(",
      :feed_list        => '[quote=Liste]',
      :feed_unknown     => 'je ne connais pas ce flux :(',
      :how_to_subscribe => 'prochaine étape :arrow: subscribe %s',
      :manual           => '[quote=Manuel]',
      :notification     => '[color=%s]%s[/color]: [b]%s[/b] %s',
      :send_pm          => '/pm %s> %s',
      :sub_created      => 'abonnement accepté :D',
      :sub_deleted      => 'abonnement annulé :cry:',
      :sub_exists       => 'tu es déjà abonné à ce flux :P',
      :sub_none         => "[i]Tu n'es pas encore abonné à un flux (subscribe <id>)[/i]",
      :sub_unknown      => 'abonnement introuvable',
      :subscribed       => ':arrow: %s',
    }.freeze

    @colors = [
      '#008080',
      '#00afff',
      '#20ad20',
      '#228b22',
      '#268bd2',
      '#2fc12f',
      '#5f87d7',
      '#5f9ea0',
      '#5faf00',
      '#5faf5f',
      '#6495ed',
      '#6b8e23',
      '#808000',
      '#8787af',
      '#87afff',
      '#8a2be2',
      '#8B4513',
      '#af5f5f',
      '#af8787',
      '#bfc2c9',
      '#d7875f',
      '#d7875f',
      '#dc143c',
      '#e6ea50',
      '#ff5d28',
      '#ff69b4',
      '#ff7f50',
      '#ffa500',
    ].freeze
  end

  def cmd_man(*args)
    yield str(:manual) + <<-MAN
    add <id> <*url>  => charge un flux
    list             => tous les flux connus
    man              => envoie une copie de ce manuel
    subscribe <id>   => activer les notifications
    unsubscribe <id> => désactiver les notifications
    MAN
  end

  alias :cmd_help :cmd_man

  def cmd_list(*args)
    if args.count != 0
      yield str(:bad_call)
      return
    end

    out = str :feed_list

    my_subs = @db.execute 'SELECT feed FROM subs WHERE nick = ?', @message.author
    my_subs.flatten!

    query = 'SELECT id, url, broadcast FROM feeds ORDER BY broadcast DESC, id'
    @db.execute query do |feed|
      id, url, broadcast = feed

      line =  "[color=#{color_for id}]#{id}[/color] => #{url}"
      line = str(:broadcasted) % line if broadcast == 1
      line = str(:subscribed) % line if my_subs.include? id
      
      out << line + "\n"
    end

    if my_subs.empty?
      out << "\n" + str(:sub_none)
    end

    yield out
  end

  def cmd_add(*args)
    if args.count < 2
      yield str(:bad_call)
      return
    end

    id = args.shift
    url = args.join "\x20"
    url = $1 if url =~ /^[^\s]+\s\((.+?)\)/

    similar = @db.get_first_row \
      'SELECT id FROM feeds WHERE id = ? OR url = ? LIMIT 1', [id, url]

    unless similar.nil?
      yield str(:feed_exists)
      yield str(:how_to_subscribe) % similar.first
      return
    end

    # yield str(:chotto_matte)
    feed = download_feed(url)

    if feed.nil?
      yield str(:feed_invalid)
      return
    end

    @db.execute 'INSERT INTO feeds (id, url) VALUES (?, ?)', [id, url]

    yield str(:feed_added) % feed.title
    yield str(:how_to_subscribe) % id
  end

  def cmd_subscribe(*args)
    if args.count != 1
      yield str(:bad_call)
      return
    end

    id = args.first
    exists = @db.get_first_row 'SELECT id FROM feeds WHERE id = ? LIMIT 1', id

    if exists.nil?
      yield str(:feed_unknown)
      return
    else
      # use the correct case afterward
      id = exists.first
    end

    subscribed = @db.get_first_row \
      'SELECT id FROM subs WHERE feed = ? AND nick = ? LIMIT 1', [id, @message.author]

    unless subscribed.nil?
      yield str(:sub_exists)
      return
    end

    @db.execute 'INSERT INTO subs (feed, nick) VALUES (?, ?)', [id, @message.author]

    yield str(:sub_created)
  end

  def cmd_unsubscribe(*args)
    if args.count != 1
      yield str(:bad_call)
      return
    end

    id = args.first
    subscribed = @db.get_first_row \
      'SELECT id FROM subs WHERE feed = ? AND nick = ? LIMIT 1', [id, @message.author]

    if subscribed.nil?
      yield str(:sub_unknown)
      return
    end

    @db.execute 'DELETE FROM subs WHERE id = ?', [subscribed.first]

    yield str(:sub_deleted)
  end

  def cmd_ping(*args)
    yield 'pong'
  end

  private
  def str(sym)
    string = @lang[sym]
    string ? string.dup : sym.to_s
  end

  def debug(message)
    puts "[#{Time.now}] #{message}"
  end

  def fetch_updates
    if @locked
      debug "a previous update is still ongoing!"
      return
    end

    @locked = true

    debug 'update started'

    @db.execute "BEGIN TRANSACTION"

    # select feeds with at least one subscription or broadcasted
    select_feeds = 'SELECT DISTINCT feeds.id, url, broadcast FROM feeds JOIN subs ON feeds.id = subs.feed OR broadcast = 1'

    @db.execute select_feeds do |row|
      id, url, broadcast = row
      feed = download_feed url

      # download error or invalid feed
      next if feed.nil?

      @db.execute "UPDATE feeds SET updated = datetime('now') WHERE id = ?", id

      cache = @db.execute 'SELECT guid FROM cache WHERE feed = ?', id
      cache.flatten!

      subs = @db.execute 'SELECT nick FROM subs WHERE feed = ?', id
      subs.flatten!

      feed.items.reverse.each {|item|
        guid = item_guid(item)
        next if cache.include? guid

        @db.execute 'INSERT INTO cache (feed, guid) VALUES (?, ?)', [id, guid]

        message =  str(:notification) %
          [color_for(id), id, item.title, real_url(item.link)]

        say_later message if broadcast == 1
        subs.each {|nick| say_later message, nick }
      }
    end

    # destroy failing or unused feeds
    select_garbage = "SELECT id FROM feeds WHERE updated < datetime('now', ?)"

    @db.execute select_garbage, Settings[:garbage] do |row|
      id = row.first

      debug "delete feed #{id}"

      @db.execute 'DELETE FROM feeds WHERE id   = ?', id
      @db.execute 'DELETE FROM subs  WHERE feed = ?', id
      @db.execute 'DELETE FROM cache WHERE feed = ?', id
    end
  ensure
    @db.execute "END TRANSACTION"
    @locked = false

    debug 'update finished'
  end

  def say_later(message, nick = nil)
    @db.execute 'INSERT INTO queue (nick, message) VALUES (?, ?)',
      [nick, message]
  end

  def read_queue
    return if @locked

    @db.execute "BEGIN TRANSACTION"

    @db.execute "SELECT nick, GROUP_CONCAT(message, '\n') FROM queue GROUP BY nick LIMIT 50" do |row|
      nick, message = row

      if message.lines.count > 1
        message = str(:big_notif) + message
      end

      if nick
        # private notification
        next unless @client.user_list.find nick

        message = str(:send_pm) % [nick, message]
        @db.execute 'DELETE FROM queue WHERE nick = ?', nick
      else
        # broadcast
        @db.execute 'DELETE FROM queue WHERE nick IS NULL'
      end

      @client.speak message
    end

    @db.execute "END TRANSACTION"
  end

  def wake
    me = @client.user_list.find Settings[:username]
    return if me.nil? || !me.sleeping?

    debug 'waking up *yawn*'
    @client.speak '/wake'
  end

  def download_feed(url)
    f = Fiber.current

    debug "downloading #{url}"

    http = EventMachine::HttpRequest.new(url).get :redirects => 2
    http.callback {
      feed = parse_feed http.response
      f.resume feed
    }
    http.errback {
      debug '=> http error: ' + http.error.to_s
      f.resume
    }

    Fiber.yield
  rescue StandardError => e
    debug "=> download error: #{e.message} (#{e.class})"
  end

  def parse_feed(data)
    feed = Feed.new
    feed.items = []

    feed_data = RSS::Parser.parse(data, false)

    if feed_data.nil?
      raise "feed is empty"
    end

    case feed_data.feed_type
    when 'rss'
      feed.title = feed_data.channel.title
      feed_data.items.each {|item_data|
        item = Item.new
        item.title = sanitize item_data.title
        item.link = item_data.link

        feed.items << item
      }
    when 'atom'
      feed.title = feed_data.title.content
      feed_data.entries.each {|item_data|
        item = Item.new
        item.title = sanitize item_data.title.content
        item.link = item_data.link ? item_data.link.href : ''

        feed.items << item
      }
    else
      raise "unknown feed type - got #{feed_data.feed_type}"
    end

    debug "=> %s: %s (%d items)" %
      [feed_data.feed_type, feed.title, feed.items.count]

    feed
  rescue StandardError => e
    debug "=> parse error: #{e.message} (#{e.class})"
  end

  def real_url(url)
    f = Fiber.current

    http = EventMachine::HttpRequest.new(url).get :redirects => 2
    http.callback { f.resume http.last_effective_url }
    http.errback  { f.resume url }

    Fiber.yield
  rescue
    url
  end

  def sanitize(string)
    @html.decode(string).gsub(/\u00A0|\n/, "\x20")
  end

  def item_guid(item)
    Digest::SHA1.hexdigest item.title + item.link
  end

  def color_for(string)
    index = string.sum % @colors.count
    @colors[index]
  end
end

client = CfiChat::Client.new Settings[:server]

hashed_pass = Digest::SHA1.hexdigest Settings[:password]
client.cookies = "username=#{Settings[:username]}; password=#{hashed_pass}"

loop do
  client.run Settings[:login_room] do
    RSS_Bot.new client
  end

  puts
  puts "Reconnecting in 30 seconds..."

  # automatic reconnection after a kick or any other fatal server error
  sleep 30
end
